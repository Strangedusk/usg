const Self = @This();

const input = @import("input.zig");

activate: bool = false,
holding: bool = false,
input: *const input.Input,
callback: *const anyopaque, // MUST be a function with void return

pub fn init(tmp_input: *const input.Input, callback: *const anyopaque) Self {
    return Self{
        .input = tmp_input,
        .callback = callback,
    };
}

pub fn call(self: Self, state: anytype) void {
    @call(.auto, @as(*const fn (@TypeOf(state)) void, @ptrCast(self.callback)), .{state});
}
