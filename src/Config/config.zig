const std = @import("std");
const fs = std.fs;
const io = std.io;

pub const Option = struct {
    key: []const u8,
    value: []const u8,
};

pub const ConfigIterator = struct {
    filename: []const u8,
    file: fs.File,
    buf: [1024]u8,
    i: i32 = 0,

    pub fn next(self: *ConfigIterator) ?Option {
        if (self.i >= 1024) {
            std.debug.print("WARNING: Maximum number of lines in config reached, ignoring the rest.\n", .{});
            return null;
        }

        var reader = self.file.reader();
        var fbs = io.fixedBufferStream(&self.buf);
        var writer = fbs.writer();

        reader.streamUntilDelimiter(writer, '\n', self.buf.len) catch |err| {
            if (err == error.EndOfStream) {
                return null;
            } else {
                std.debug.print("ConfigIterator Error: {any}\n", .{err});
                unreachable;
            }
        };

        var line = fbs.getWritten();

        var keyval = std.mem.split(u8, line, "=");

        var result = Option{
            .key = keyval.next() orelse {
                std.debug.print("Config {s}: line {} with contents '{s}' is invalid. Skipping.\n", .{
                    self.filename,
                    self.i,
                    line,
                });
                self.i += 1;
                return self.next();
            },
            .value = keyval.next() orelse {
                std.debug.print("Config {s}: line {} with contents '{s}' is invalid. Skipping.\n", .{
                    self.filename,
                    self.i,
                    line,
                });
                self.i += 1;
                return self.next();
            },
        };

        if (keyval.next() != null) {
            std.debug.print("Config {s}: line {} with contents '{s}' is invalid. Skipping.\n", .{
                self.filename,
                self.i,
                line,
            });
            self.i += 1;
            return self.next();
        }

        self.i += 1;
        return result;
    }
    pub fn deinit(self: *ConfigIterator) void {
        self.file.close();
    }
};

pub fn getConfigIterator(path: []const u8) !ConfigIterator {
    var iterator = ConfigIterator{
        .filename = path,
        .file = try fs.cwd().createFile(path, .{ .read = true, .truncate = false }),
        .buf = undefined,
    };

    return iterator;
}
