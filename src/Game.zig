const std = @import("std");

const Grid = @import("Game/Grid.zig");
const Piece = @import("Game/Piece.zig");
const Bag = @import("Game/Bag.zig");
const Timer = @import("Timer.zig");
const State = @import("flow.zig").State;
const Action = @import("Action.zig");
const input = @import("input.zig");

const Renderer = @import("Game/Renderer.zig");
const movement = @import("Game/movement.zig");

pub const Score = u64;

const Self = @This();

// Codes to access action list
const ActionList = struct {
    right: Action,
    left: Action,
    down: Action,
    hard: Action,
    swap: Action,
    rot_r: Action,
    rot_l: Action,
};

grid: Grid,

renderer: Renderer,

state: State = State.game,

bag: Bag,

held: ?Piece.Type,
piece: Piece,
shadow: Piece,

score: Score = 0,

action_list: ActionList = .{
    .right = Action.init(&input.game_right, actionRight), // Right
    .left = Action.init(&input.game_left, actionLeft), // Left
    .down = Action.init(&input.game_down, actionDown), // Down
    .hard = Action.init(&input.game_drop, actionHard), // Instant Drop
    .swap = Action.init(&input.game_swap_piece, actionSwap), // Swap Piece
    .rot_r = Action.init(&input.game_rotate_r, actionRotR), // Rotate
    .rot_l = Action.init(&input.game_rotate_l, actionRotL), // Rotate
},

timer_down: Timer,
timer_left_arr: Timer,
timer_left_das: Timer,
timer_right_arr: Timer,
timer_right_das: Timer,
timer_gravity: Timer,
timer_line_clear: Timer,

needs_reinit: bool = false,

pub fn init(renderer: *Renderer.Renderer) Self {
    var ret = Self{
        .grid = Grid.init(),

        .bag = Bag.init(),
        .piece = undefined,
        .shadow = undefined,
        .held = null,
        .renderer = Renderer.init(renderer),

        .timer_down = Timer.init(0.05),
        .timer_left_arr = Timer.init(0.05),
        .timer_left_das = Timer.init(0.2),
        .timer_right_arr = Timer.init(0.05),
        .timer_right_das = Timer.init(0.2),
        .timer_gravity = Timer.init(0.3),
        .timer_line_clear = Timer.init(0.25),
    };

    // Get a piece from the bag
    ret.piece = ret.bag.pop();

    return ret;
}

// Main Game loop
pub fn tick(self: *Self) State {
    if (self.timer_line_clear.started and !self.timer_line_clear.finished()) {
        self.renderer.render(self.*);
        return self.state;
    }

    // TIMERS
    // Dropping a piece
    if (self.piece.timer_dropped.finished()) {
        self.piece.timer_dropped.stop();
        self.piece.dropped = true;
    }
    // Repeat Right

    // DAS
    if (self.action_list.right.holding) {
        if (!self.timer_right_das.started) {
            self.timer_right_das.start();
        }
    } else {
        // Stop both
        self.timer_right_das.stop();
        self.timer_right_arr.stop();
    }

    // ARR
    if (self.timer_right_das.finished()) {
        if (!self.timer_right_arr.started) {
            self.timer_right_arr.start();
        } else if (self.timer_right_arr.finished()) {
            self.timer_right_arr.stop();
            self.action_list.right.holding = false;
        }
    }

    // Repeat Left

    // DAS
    if (self.action_list.left.holding) {
        if (!self.timer_left_das.started) {
            self.timer_left_das.start();
        }
    } else {
        // Stop both
        self.timer_left_das.stop();
        self.timer_left_arr.stop();
    }

    // ARR
    if (self.timer_left_das.finished()) {
        if (!self.timer_left_arr.started) {
            self.timer_left_arr.start();
        } else if (self.timer_left_arr.finished()) {
            self.timer_left_arr.stop();
            self.action_list.left.holding = false;
        }
    }

    // Repeat Down
    if (self.action_list.down.holding) {
        if (!self.timer_down.started) {
            self.timer_down.start();
        } else if (self.timer_down.finished()) {
            self.timer_down.stop();
            self.action_list.down.holding = false;
        }
    } else {
        self.timer_down.stop();
    }

    // Gravity

    if (!self.piece.timer_dropped.started) {
        if (!self.timer_gravity.started) {
            self.timer_gravity.start();
        } else if (self.timer_gravity.finished()) {
            self.action_list.down.activate = true;
            self.timer_gravity.start();
        }
    } else {
        self.timer_gravity.stop();
    }

    // DROP

    if (self.piece.dropped) {
        self.grid = movement.drop(self.grid, self.piece);
        // New Piece
        self.piece = self.bag.pop();
    }

    // KEY EVENTS
    const action_fields = std.meta.fields(ActionList);
    inline for (action_fields) |field| {
        // REVIEW: Is this necessary?
        const action = &@field(self.action_list, field.name);

        if (action.input.isDown() and !action.*.holding) {
            action.*.holding = true;
            action.*.activate = true;
        }
        if (!action.input.isDown()) {
            action.*.holding = false;
        }

        // Action
        if (action.*.activate) {
            action.*.activate = false;
            action.*.call(self);
        }
    }

    // Update Shadow
    {
        self.shadow = movement.shadow(self.grid, self.piece);
        self.shadow.color[3] /= 4;
    }

    // Check for a top out
    {
        if (movement.isToppedOut(self.grid)) {
            std.debug.print("Topped Out!\n", .{});
            // Return to main menu
            self.state = State.main_menu;
            // Flag that I need a reset
            self.needs_reinit = true;
        }
    }

    // Check for empty lines. If there are, then start the clearing sequence
    if (!self.timer_line_clear.started) {
        if (self.grid.updateLinesToClear()) {
            self.timer_line_clear.start();
        }
    }
    // Don't go and clear them until the timer has finished
    else if (self.timer_line_clear.finished()) {
        const clear_score = self.grid.clearLines();
        if (clear_score > 0) {
            self.timer_line_clear.stop();
            self.score += clear_score;
        } else {
            std.debug.print("WARNING: Clear line timer stopped but there was nothing to clear. This situation isn't valid\n", .{});
        }
    }
    // NOTE: If for some reason game doesn't stop and another line has been cleared
    //       before the timer reached the end, that line will be processed right after
    //       the timer stops. Behaviour shouldn't get any funkier than that.

    self.renderer.render(self.*);

    return self.state;
}

// ACTION CALLBACKS

fn actionDown(self: *Self) void {
    self.piece = movement.moveDown(self.grid, self.piece);
}

fn actionRight(self: *Self) void {
    self.piece = movement.moveRight(self.grid, self.piece);
}

fn actionLeft(self: *Self) void {
    self.piece = movement.moveLeft(self.grid, self.piece);
}
fn actionHard(self: *Self) void {
    self.piece = movement.hardDrop(self.grid, self.piece);
}
fn actionSwap(self: *Self) void {
    if (self.held) |held_piece| {
        if (!self.piece.swapped) {
            self.held = self.piece.piece_type;
            self.piece = Piece.init(held_piece);
            self.piece.swapped = true;
        }
    } else {
        self.held = self.piece.piece_type;
        self.piece = self.bag.pop();
        self.piece.swapped = true;
    }
}
fn actionRotR(self: *Self) void {
    self.piece = movement.rotateRight(self.grid, self.piece);
}
fn actionRotL(self: *Self) void {
    self.piece = movement.rotateLeft(self.grid, self.piece);
}
