const std = @import("std");

const color = @import("../color.zig");

const Self = @This();

free: bool,
color: color.Color,

pub fn init() Self {
    return Self{
        .free = true,
        .color = color.dark_grey,
    };
}
