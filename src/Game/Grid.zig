const std = @import("std");
const SDL = @import("sdl2");

const color = @import("../color.zig");
const Cell = @import("Cell.zig");
const utils = @import("../utils.zig");

const range = utils.range;

const Self = @This();

pub const ncolumns = 10;
pub const nrows = 30;
pub const buffer = 10;

cells: [nrows][ncolumns]Cell,
topped: bool = false,
lines_to_clear: [nrows]bool = .{false} ** nrows,

pub fn init() Self {
    var self = Self{
        .cells = undefined,
    };

    for (self.cells, 0..) |_, i| {
        for (self.cells[i], 0..) |_, j| {
            self.cells[i][j] = Cell.init();
        }
    }

    return self;
}

pub fn updateLinesToClear(self: *Self) bool {
    var ret: bool = false;

    // Look at each row
    for (self.cells, 0..) |_, y| {
        // Look at each cell in the line
        for (self.cells[y], 0..) |cell_x, x| {
            if (cell_x.free) {
                // It is free, this line isn't to be cleared
                break;
            } else {
                // Reached the end of the column?
                if (x == self.cells[y].len - 1) {
                    self.lines_to_clear[y] = true;
                    ret = true;
                }
            }
        }
    }

    return ret;
}

pub fn clearLines(self: *Self) u8 {
    var ncleared: u8 = 0;

    // Go line by line, checking if any of them needs to be cleared
    for (self.lines_to_clear, 0..) |_, y| {
        if (self.lines_to_clear[y]) {
            // Delete current row and bring the others down
            for (self.cells[1..y], 0..) |_, i| {
                self.cells[y - i] = self.cells[y - i - 1];
            }
            ncleared += 1;
        }
    }

    self.lines_to_clear = .{false} ** nrows;

    return ncleared;
}
