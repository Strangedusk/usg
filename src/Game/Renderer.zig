const std = @import("std");
const Game = @import("../Game.zig");
const Bag = @import("Bag.zig");
const Grid = @import("Grid.zig");
const Piece = @import("Piece.zig");
pub const Renderer = @import("../Renderer.zig");

const color = @import("../color.zig");

const Self = @This();

renderer: *Renderer,
grid_cell_size: i32,
grid_pos_x: i32,
grid_pos_y: i32,
cell_texture: Renderer.Texture,

pub fn init(renderer: *Renderer) Self {
    var wsize = renderer.getOutputSize();

    const grid_cell_size = @divFloor(@min(wsize.width, wsize.height), 32);
    const grid_pos_x = @divFloor(wsize.width, 2) - (grid_cell_size * @divFloor(Grid.ncolumns, 2));
    const grid_pos_y = @divFloor(wsize.height, 2) - (grid_cell_size * @divFloor(Grid.nrows + Grid.buffer, 2));
    return Self{
        .renderer = renderer,
        .grid_pos_x = grid_pos_x,
        .grid_pos_y = grid_pos_y,
        .grid_cell_size = grid_cell_size,
        .cell_texture = Renderer.Texture.init("res/cell.png"),
    };
}

pub fn renderBag(self: *Self, game: Game) void {
    const pos_x = self.grid_pos_x + ((Grid.ncolumns + 1) * self.grid_cell_size);
    const pos_y = self.grid_pos_y + (Grid.buffer * self.grid_cell_size);

    for (game.bag.contents[0..5], 0..) |_, i| {
        if (game.bag.contents[i]) |piece_type| {
            var piece = Piece.init(piece_type);

            for (piece.structure, 0..) |_, y| {
                for (piece.structure[y], 0..) |_, x| {
                    const new_x = pos_x + @as(i32, @intCast(x)) * self.grid_cell_size;
                    const new_y = pos_y + (@as(i32, @intCast(y)) + @as(i32, @intCast(i * piece.structure.len))) * self.grid_cell_size;

                    if (piece.structure[y][x]) {
                        self.cell_texture.drawTo(new_x, new_y, self.grid_cell_size, self.grid_cell_size, piece.color);
                    } else {
                        const alpha = 50;

                        self.renderer.setColor([_]u8{
                            piece.color[0] -| 30, // r
                            piece.color[1] -| 30, // g
                            piece.color[2] -| 30, // b
                            alpha,
                        });
                        self.renderer.fillRectangle(new_x, new_y, self.grid_cell_size, self.grid_cell_size);

                        self.renderer.setColor(.{ piece.color[0], piece.color[1], piece.color[2], alpha });
                        self.renderer.fillRectangle(new_x + 1, new_y + 1, self.grid_cell_size - 2, self.grid_cell_size - 2);
                    }
                }
            }
        }
    }
}

pub fn renderHeld(self: *Self, game: Game) void {
    const pos_x = self.grid_pos_x - (5 * self.grid_cell_size);
    const pos_y = self.grid_pos_y + (Grid.buffer * self.grid_cell_size);

    if (game.held) |held_type| {
        var held = Piece.init(held_type);

        for (held.structure, 0..) |_, y| {
            for (held.structure[y], 0..) |_, x| {
                const new_x = pos_x + @as(i32, @intCast(x)) * self.grid_cell_size;
                const new_y = pos_y + @as(i32, @intCast(y)) * self.grid_cell_size;

                if (held.structure[y][x]) {
                    self.cell_texture.drawTo(new_x, new_y, self.grid_cell_size, self.grid_cell_size, held.color);
                } else {
                    const alpha = 50;

                    self.renderer.setColor([_]u8{
                        held.color[0] -| 30, // r
                        held.color[1] -| 30, // g
                        held.color[2] -| 30, // b
                        alpha,
                    });
                    self.renderer.fillRectangle(new_x, new_y, self.grid_cell_size, self.grid_cell_size);

                    self.renderer.setColor(.{ held.color[0], held.color[1], held.color[2], alpha });
                    self.renderer.fillRectangle(new_x + 1, new_y + 1, self.grid_cell_size - 2, self.grid_cell_size - 2);
                }
            }
        }
    }
}

pub fn render(self: *Self, game: Game) void {
    renderGrid(self, game);
    renderPiece(self, game.piece);
    renderPiece(self, game.shadow);
    renderBag(self, game);
    renderHeld(self, game);
    renderScore(self, game.score);
}

pub fn renderPiece(self: *Self, piece: Piece) void {
    const pos_x = self.grid_pos_x;
    const pos_y = self.grid_pos_y;

    for (piece.structure, 0..) |_, y| {
        for (piece.structure[y], 0..) |_, x| {
            // We don't want to paint void cells
            if (piece.structure[y][x] == false) {
                continue;
            }

            const new_x = pos_x + (@as(i32, @intCast(x)) + piece.col) * self.grid_cell_size;
            const new_y = pos_y + (@as(i32, @intCast(y)) + piece.row) * self.grid_cell_size;

            self.cell_texture.drawTo(new_x, new_y, self.grid_cell_size, self.grid_cell_size, piece.color);
        }
    }
}

pub fn renderGrid(self: *Self, game: Game) void {
    const pos_x = self.grid_pos_x;
    const pos_y = self.grid_pos_y;

    //const clear_alpha: u8 = @intFromFloat(255.0 * game.timer_line_clear.completionRatio());
    const clear_alpha: u8 = @intFromFloat(255.0 - 255.0 * game.timer_line_clear.completionRatio());

    var visible = game.grid.cells[Grid.buffer..];
    var visible_to_clear = game.grid.lines_to_clear[Grid.buffer..];
    for (visible, 0..) |_, y| {
        // Set the lines to be cleared with the desired transparency
        const line_alpha: u8 = if (visible_to_clear[y])
            clear_alpha
        else
            255;

        for (visible[y], 0..) |_, x| {
            const new_x = pos_x + @as(i32, @intCast(x)) * self.grid_cell_size;
            const new_y = pos_y + (@as(i32, @intCast(y)) + Grid.buffer) * self.grid_cell_size;

            // -- Render background cells --
            // Fill
            // TODO: Render this as a big rectangle beforehand
            self.renderer.setColor(color.light_grey);
            self.renderer.fillRectangle(new_x, new_y, self.grid_cell_size, self.grid_cell_size);
            // outline
            self.renderer.setColor(color.dark_grey);
            self.renderer.fillRectangle(new_x + 1, new_y + 1, self.grid_cell_size - 2, self.grid_cell_size - 2);

            // Render dropped cells
            if (!visible[y][x].free) {
                self.cell_texture.drawTo(new_x, new_y, self.grid_cell_size, self.grid_cell_size, .{
                    visible[y][x].color[0],
                    visible[y][x].color[1],
                    visible[y][x].color[2],
                    line_alpha,
                });
            }
        }
    }
}

pub fn renderScore(self: *Self, score: Game.Score) void {
    const s_size = self.renderer.getOutputSize();

    var buffer: [32]u8 = undefined;
    var score_string = std.fmt.bufPrintZ(&buffer, "Score: {d:0>16}", .{score}) catch "Score: " ++ "9" ** 16;

    self.renderer.setColor(.{ 0, 0, 0, 255 });
    self.renderer.drawText(score_string, s_size.width - 400, 50, 20);
}
