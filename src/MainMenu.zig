const std = @import("std");
//const SDL = @import("sdl2");
const range = @import("utils.zig").range;

const MenuSelection = @import("MainMenu/MenuSelection.zig");
const MenuTab = @import("MainMenu/MenuTab.zig");
const Renderer = @import("MainMenu/Renderer.zig");

const State = @import("flow.zig").State;
const Action = @import("Action.zig");

const input = @import("input.zig");

const Self = @This();

const ActionList = struct {
    right: Action,
    left: Action,
    down: Action,
    up: Action,
    select: Action,
};

action_list: ActionList = .{
    .right = Action.init(&input.menu_right, actionTabRight), // Tab Right
    .left = Action.init(&input.menu_left, actionTabLeft), // Tab left
    .down = Action.init(&input.menu_down, actionSelDown), // Go down
    .up = Action.init(&input.menu_up, actionSelUp), // Go up
    .select = Action.init(&input.menu_accept, actionSelect), // Select
},

tab_list: [3]MenuTab = .{
    MenuTab.init("1P", .{ 245, 155, 20 }, &.{
        MenuSelection.init("Play", play),
        MenuSelection.init("Print", print),
    }),
    MenuTab.init("Options", .{ 162, 86, 179 }, &.{
        MenuSelection.init("Play", play),
        MenuSelection.init("Print", print),
    }),
    MenuTab.init("Online", .{ 105, 179, 86 }, &.{
        MenuSelection.init("Play", play),
        MenuSelection.init("Print", print),
        // FIXME: Crashes upon trying to render them
        //MenuSelection.init("Print", print),
        //MenuSelection.init("Print", print),
    }),
},

renderer: Renderer,

// Current tab and selection index
tab: usize = 0,

state: State = State.main_menu,

holding_down: bool = false,
holding_enter: bool = false,

needs_reinit: bool = false,

pub fn init(renderer: *Renderer.Renderer) Self {
    return Self{ .renderer = Renderer.init(renderer) };
}

pub fn getSel(self: Self, offset: i32) MenuSelection {
    return self.tab_list[self.tab].contents[self.getSelIdx(offset)];
}

pub fn getSelIdx(self: Self, offset: i32) usize {
    const len = @as(i32, @intCast(self.getTab().contents.len));
    const sel = @as(i32, @intCast(self.getTab().sel));

    var ret: i32 = undefined;
    if (offset < 0) {
        ret = sel;

        for (range(@as(usize, @intCast(-offset)))) |_| {
            ret -= 1;
            if (ret < 0) {
                ret = len - 1;
            }
        }
        //ret = len - @mod(-offset - sel, len - 1);
        //ret = @mod(0, 2);
        //std.debug.print("len {},offset {},ret {}, sel {}\n", .{ len, offset, ret, sel });
    } else {
        ret = @mod(offset + sel, len);
        //std.debug.print("len {},offset {},ret {}, sel {}\n", .{ len, offset, ret, sel });
    }

    return @as(usize, @intCast(ret));
}

pub fn getTab(self: Self) MenuTab {
    return self.tab_list[self.tab];
}

pub fn tick(self: *Self) State {
    if (self.state != State.main_menu) {
        // If we are returnining
        self.state = State.main_menu;
    }

    //const sel = self.getSel();
    //const tab = self.getTab();

    const action_fields = std.meta.fields(ActionList);
    inline for (action_fields) |field| {
        // REVIEW: Is this necessary?
        const action = &@field(self.action_list, field.name);

        if (action.input.isDown() and !action.*.holding) {
            action.*.holding = true;
            action.*.activate = true;
        }
        if (!action.input.isDown()) {
            action.*.holding = false;
        }

        // Action
        if (action.*.activate) {
            action.*.activate = false;
            action.*.call(self);
        }
    }

    //std.debug.print(
    //\\Tab: {s}
    //\\Selection: {s}
    //\\
    //, .{ tab.name, sel.name });

    self.renderer.render(self.*);
    return self.state;
}

pub fn getPrevTab(self: Self) usize {
    if (self.tab == 0) {
        return self.tab_list.len - 1;
    } else {
        return self.tab - 1;
    }
}

pub fn getNextTab(self: Self) usize {
    if (self.tab == self.tab_list.len - 1) {
        return 0;
    } else {
        return self.tab + 1;
    }
}

fn actionTabLeft(self: *Self) void {
    self.tab = self.getPrevTab();
}

fn actionTabRight(self: *Self) void {
    self.tab = self.getNextTab();
}

fn actionSelDown(self: *Self) void {
    self.tab_list[self.tab].sel = self.getSelIdx(1);
}

fn actionSelUp(self: *Self) void {
    // if (self.tab_list[self.tab].sel == 0) {
    //     self.tab_list[self.tab].sel = self.tab_list[self.tab].contents.len - 1;
    // } else {
    //     self.tab_list[self.tab].sel -= 1;
    // }
    self.tab_list[self.tab].sel = self.getSelIdx(-1);
}

fn actionSelect(self: *Self) void {
    const action = self.getSel(0).action;
    @call(.auto, action, .{self});
}

fn play(self: *Self) void {
    self.state = State.game;
}

fn print(self: *Self) void {
    _ = self;
    std.debug.print("Print\n", .{});
}
