const MainMenu = @import("../MainMenu.zig");

const Self = @This();

name: [:0]const u8,
action: *const fn (*MainMenu) void,

pub fn init(name: [:0]const u8, action: *const fn (*MainMenu) void) Self {
    return Self{
        .name = name,
        .action = action,
    };
}
