const std = @import("std");

const MenuSelection = @import("MenuSelection.zig");

const Self = @This();

name: []const u8,
contents: []MenuSelection,
contents_buffer: [50]MenuSelection,
color: [3]u8, // Make this const
sel: usize = 0,

pub fn init(name: []const u8, color: [3]u8, contents: []const MenuSelection) Self {
    var ret = Self{
        .name = name,
        .contents = undefined,
        .contents_buffer = undefined,
        .color = color,
    };
    std.mem.copy(MenuSelection, &ret.contents_buffer, contents);
    ret.contents = ret.contents_buffer[0..contents.len];
    return ret;
}
