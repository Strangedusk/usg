const std = @import("std");
const SDL = @import("sdl2");

const MainMenu = @import("../MainMenu.zig");
const MenuTab = @import("MenuTab.zig");

const range = @import("../utils.zig").range;

pub const Renderer = @import("../Renderer.zig");
const color = @import("../color.zig");

const Self = @This();

renderer: *Renderer,

const skew = 13;
const y_spacing = 20;
const x_spacing = 300;
const width = 300;
const height = 60;

pub fn init(renderer: *Renderer) Self {
    return Self{
        .renderer = renderer,
    };
}
pub fn render(self: Self, main_menu: MainMenu) void {
    const tabs = [_]usize{
        main_menu.getPrevTab(),
        main_menu.tab,
        main_menu.getNextTab(),
    };

    const wsize = self.renderer.getOutputSize();

    for (tabs, 0..) |u_tab, u_tab_i| {
        //const tab = @intCast(i32, u_tab);
        const tab_i = @as(i32, @intCast(u_tab_i));
        const curr_tab = main_menu.tab_list[u_tab];

        // Auxiliary variables to claculate the center of the screen
        const total_spacing: i32 = (tabs.len - 1) * x_spacing;
        const total_width: i32 = tabs.len * width;

        // Current selection vertical offset
        const sel_y_offset = y_spacing * 12;
        // Number of items below selection
        const n_sel_below = @as(usize, @intCast(@divExact((wsize.height - sel_y_offset), (height + y_spacing))));
        // Number of items below selection
        const n_sel_above = @as(usize, @intCast(@divExact((sel_y_offset), (height + y_spacing))));

        // Move it from the left to the center
        const centering = @divExact((wsize.width - (total_width + total_spacing)), 2);

        const x = tab_i * (width + x_spacing) + centering;

        // Transparency depending on tab at the middle
        var alpha: u8 = 255;
        if (tab_i == 1) {
            alpha = 255;
        }

        // Current selection
        {
            const y = @as(i32, @intCast(sel_y_offset));

            // TODO: The shadow should be static, it is easier like this rn tho

            if (tab_i == 1) {
                // Shadow
                self.renderer.setColor(.{ 0, 0, 0, 30 });
                self.renderer.fillRectangleEx(x + 10, y + 10, width, height, skew);
            }

            self.renderMenu(x, y, curr_tab, curr_tab.sel, alpha, true);
        }

        for (range(n_sel_below), 0..) |_, i| {
            const aux_sel: i32 = @as(i32, @intCast(i + 1));
            const curr_sel: usize = main_menu.getSelIdx(aux_sel);
            const y = @as(i32, @intCast(sel_y_offset + ((y_spacing + height) * (aux_sel))));
            self.renderMenu(x - ((skew + 8) * aux_sel), y, curr_tab, curr_sel, alpha, false);
        }

        for (range(n_sel_above), 0..) |_, i| {
            const aux_sel: i32 = -@as(i32, @intCast(i + 1));
            const curr_sel: usize = main_menu.getSelIdx(aux_sel);
            const y = @as(i32, @intCast(sel_y_offset + ((y_spacing + height) * (aux_sel))));
            self.renderMenu(x - ((skew + 8) * aux_sel), y, curr_tab, curr_sel, alpha, false);
        }

        // Tab header
        self.renderer.setColor(.{ curr_tab.color[0], curr_tab.color[1], curr_tab.color[2], alpha });
        self.renderer.fillRectangleEx(x - 25, 10, width + 50, height + 50, skew);
    }

    // TODO: Set the Color depending on the Main Menu color
    self.renderer.setColor(.{ 232, 217, 166, 127 });
    self.renderer.fillRectangleEx(-150, 0, @divTrunc(wsize.width, 3), wsize.height, skew);
    self.renderer.fillRectangleEx(wsize.width - 300, 0, @divTrunc(wsize.width, 3), wsize.height, skew);
}

fn renderMenu(self: Self, x: i32, y: i32, tab: MenuTab, sel: usize, a: u8, selected: bool) void {
    // White background
    self.renderer.setColor(.{ 255, 255, 255, a });
    self.renderer.fillRectangleEx(x, y, width, height, skew);

    if (selected) {
        // Set color if selected
        self.renderer.setColor(.{ tab.color[0], tab.color[1], tab.color[2], a });
    } else {
        // Set black color, not selected
        self.renderer.setColor(.{ 0, 0, 0, a });
    }

    const margin = 20;

    self.renderer.drawText(
        tab.contents[sel].name,
        x + margin,
        y + margin,
        height - margin * 2,
    );

    //self.renderer.fillRectangleEx(x + margin, y + margin, width - margin * 2 - 12, height - margin * 2, skew);
    self.renderer.fillRectangleEx(x + width - 6, y, 6, height, skew);
    self.renderer.fillRectangleEx(x + width - 12, y, 3, height, skew);
}
