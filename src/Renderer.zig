const std = @import("std");
const rl = @import("raylib.zig");

const Self = @This();
const colors = @import("color.zig");

color: colors.Color = .{ 0, 0, 0, 0 },

const max_objects: usize = 16384;
const quadSize: usize = 9 * 6;

pub fn init() !Self {
    rl.ConfigFlag.msaa_4x_hint.set();
    rl.initWindow(1280, 720, "USG", 120);

    return Self{};
}

pub fn render(self: *Self) void {
    _ = self;
    rl.drawFPS(10, 10);

    rl.endDrawing();
    rl.beginDrawing();

    rl.clearBackground(.{ 232, 216, 166, 255 });
}

pub fn deinit(self: *Self) void {
    _ = self;

    rl.closeWindow();
}

pub fn fillRectangle(self: *Self, x: i32, y: i32, w: i32, h: i32) void {
    self.fillRectangleEx(x, y, w, h, 0);
}

pub fn fillRectangleEx(self: *Self, x: i32, y: i32, w: i32, h: i32, skew_x: i32) void {
    var xf: f32 = @floatFromInt(x);
    var yf: f32 = @floatFromInt(y);
    var wf: f32 = @floatFromInt(w);
    var hf: f32 = @floatFromInt(h);

    const skew_x_offset = @as(f32, @floatFromInt(skew_x)) * hf / 100;

    const upLeft = [_]f32{ xf + skew_x_offset, yf };
    const upRight = [_]f32{ xf + wf + skew_x_offset, yf };
    const downLeft = [_]f32{ xf - skew_x_offset, yf + hf };
    const downRight = [_]f32{ xf + wf - skew_x_offset, yf + hf };

    rl.drawTriangle(upLeft, downLeft, upRight, self.color);
    rl.drawTriangle(downLeft, downRight, upRight, self.color);
}

pub fn drawText(self: Self, text: [:0]const u8, x: i32, y: i32, size: i32) void {
    rl.drawText(text, x, y, size, self.color);
}

pub fn setColor(self: *Self, color: colors.Color) void {
    self.color = color;
}

pub const OutputSize = struct { width: i32, height: i32 };
pub fn getOutputSize(self: Self) OutputSize {
    _ = self;
    return OutputSize{
        .width = rl.getScreenWidth(),
        .height = rl.getScreenHeight(),
    };
}

pub const Texture = struct {
    texture: rl.Texture,

    pub fn init(path: [*]const u8) Texture {
        return .{
            .texture = rl.Texture.load(path),
        };
    }

    pub fn drawTo(self: Texture, x: i32, y: i32, w: i32, h: i32, tint: [4]u8) void {
        self.texture.drawTo(x, y, w, h, tint);
    }

    pub fn deinit(self: *Texture) void {
        self.texture.unload();
    }
};
