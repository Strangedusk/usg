const Self = @This();

const getTime = @import("raylib.zig").getTime;

started: bool,
initial: f64,
target: f64,

pub fn init(target: f64) Self {
    return Self{
        .started = false,
        .initial = 0,
        .target = target,
    };
}

pub fn start(self: *Self) void {
    self.started = true;
    self.initial = getTime();
}

pub fn finished(self: Self) bool {
    if (self.started) {
        if ((getTime() - self.initial) >= self.target) {
            return true;
        } else return false;
    } else {
        return false;
    }
}

pub fn reset(self: *Self) void {
    self.initial = getTime();
}

pub fn stop(self: *Self) void {
    self.started = false;
}

pub fn completionRatio(self: Self) f64 {
    if (!self.started) {
        return 0.0;
    }
    if (self.finished()) {
        return 1.0;
    }

    const elapsed = getTime() - self.initial;

    return elapsed / @max(self.target, 0.00001);
}
