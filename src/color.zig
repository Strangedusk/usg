pub const Color = [4]u8;

pub const yellow = Color{ 255, 208, 95, 255 };
pub const brown = Color{ 180, 130, 90, 255 };
pub const cyan = Color{ 138, 167, 172, 255 };
pub const orange = Color{ 222, 154, 40, 255 };
pub const blue = Color{ 112, 123, 136, 255 };
pub const green = Color{ 142, 146, 87, 255 };
pub const red = Color{ 229, 93, 77, 255 };
pub const purple = Color{ 180, 171, 189, 255 };
pub const pink = Color{ 230, 115, 170, 255 };
pub const dark_grey = Color{ 40, 40, 40, 255 };
pub const light_grey = Color{ 80, 80, 80, 255 };
