const Self = @This();

const rl = @import("raylib.zig");
const std = @import("std");
const config = @import("Config/config.zig");

pub const Input = struct {
    name: []const u8,
    code: i32,
    default: i32,

    pub fn isDown(self: Input) bool {
        return rl.isKeyDown(self.code);
    }
    pub fn isJustPressed(self: Input) bool {
        return rl.isKeyPressed(self.code);
    }
    pub fn isJustReleased(self: Input) bool {
        return rl.isKeyReleased(self.code);
    }
};


pub fn loadConfig() void {
    var iter = config.getConfigIterator("input.ini") catch {
        std.debug.print("Error loading input config, using defaults...\n", .{});
        return;
    };

    while (iter.next()) |option| {
        inline for (comptime getInputNames()) |name| {
            if (std.mem.eql(u8, option.key, name)) {
                if (rl.getValueFromInputName(option.value)) |value| {
                    @field(Self, name).code = value;
                } else {
                    std.debug.print("Invalid input value '{s}' in config, ignoring.\n", .{option.value});
                }
            }
        }
    }
}

fn init(comptime name: []const u8, comptime key_code: []const u8) Input {
    return Input{
        .name = name,
        .code = rl.getKeyCode(key_code),
        .default = rl.getKeyCode(key_code),
    };
}

pub var menu_right = init("Menu Right", "RIGHT");
pub var menu_left = init("Menu Left", "LEFT");
pub var menu_down = init("Menu Down", "DOWN");
pub var menu_up = init("Menu Up", "UP");
pub var menu_accept = init("Menu Accept", "ENTER");
pub var menu_cancel = init("Menu Cancel", "BACKSPACE");

pub var game_right = init("Move Right", "D");
pub var game_left = init("Move Left", "A");
pub var game_down = init("Move Down", "S");
pub var game_drop = init("Drop Piece", "W");
pub var game_swap_piece = init("Swap Piece", "SPACE");
pub var game_rotate_r = init("Rotate Piece Clockwise", "RIGHT");
pub var game_rotate_l = init("Rotate Piece Counterclockwise", "LEFT");

// Get the names of the Input(s) defined in this file with introspection
fn getInputNames() [getInputCount()][]const u8 {
    comptime {
        const decls = std.meta.declarations(Self);

        var ret: [getInputCount()][]const u8 = undefined;

        var i: usize = 0;
        inline for (decls) |i_decl| {
            const field = @field(Self, i_decl.name);

            if (@TypeOf(field) == Input) {
                ret[i] = i_decl.name;
                i += 1;
            }
        }

        return ret;
    }
}

// With reflection count the amount of declarations of type "Input"
fn getInputCount() usize {
    comptime {
        const decls = std.meta.declarations(Self);

        var i: usize = 0;
        inline for (decls) |i_decl| {
            const field = @field(Self, i_decl.name);

            if (@TypeOf(field) == Input) i += 1;
        }

        return i;
    }
}
