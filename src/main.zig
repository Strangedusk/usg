const std = @import("std");
// TODO: Get this from a input class, not from rl directly
const shouldClose = @import("raylib.zig").windowShouldClose;

const Renderer = @import("Renderer.zig");
const Game = @import("Game.zig");
const MainMenu = @import("MainMenu.zig");
const config = @import("Config/config.zig");

const State = @import("flow.zig").State;
const input = @import("input.zig");

pub fn main() !void {
    input.loadConfig();

    var renderer = try Renderer.init();
    defer renderer.deinit();

    var main_menu = MainMenu.init(&renderer);
    var game = Game.init(&renderer);

    var current_state: State = main_menu.state;

    while (!shouldClose()) {
        if (game.needs_reinit) {
            game = Game.init(&renderer);
        }

        if (main_menu.needs_reinit) {
            main_menu = MainMenu.init(&renderer);
        }

        current_state = switch (current_state) {
            .main_menu => main_menu.tick(),
            .game => game.tick(),
        };

        renderer.render();
    }
}
