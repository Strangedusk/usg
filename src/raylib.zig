pub const c = @cImport({
    @cInclude("raylib.h");
});

const std = @import("std");

const InputCode = struct {
    name: []const u8 = "",
    value: i32 = 0,
};

// zig fmt: off
const input_codes = [_]InputCode{
    .{ .name = "NULL"           , .value = 0,  },
    .{ .name = "APOSTROPHE"     , .value = 39, },
    .{ .name = "COMMA"          , .value = 44, },
    .{ .name = "MINUS"          , .value = 45, },
    .{ .name = "PERIOD"         , .value = 46, },
    .{ .name = "SLASH"          , .value = 47, },
    .{ .name = "ZERO"           , .value = 48, },
    .{ .name = "ONE"            , .value = 49, },
    .{ .name = "TWO"            , .value = 50, },
    .{ .name = "THREE"          , .value = 51, },
    .{ .name = "FOUR"           , .value = 52, },
    .{ .name = "FIVE"           , .value = 53, },
    .{ .name = "SIX"            , .value = 54, },
    .{ .name = "SEVEN"          , .value = 55, },
    .{ .name = "EIGHT"          , .value = 56, },
    .{ .name = "NINE"           , .value = 57, },
    .{ .name = "SEMICOLON"      , .value = 59, },
    .{ .name = "EQUAL"          , .value = 61, },
    .{ .name = "A"              , .value = 65, },
    .{ .name = "B"              , .value = 66, },
    .{ .name = "C"              , .value = 67, },
    .{ .name = "D"              , .value = 68, },
    .{ .name = "E"              , .value = 69, },
    .{ .name = "F"              , .value = 70, },
    .{ .name = "G"              , .value = 71, },
    .{ .name = "H"              , .value = 72, },
    .{ .name = "I"              , .value = 73, },
    .{ .name = "J"              , .value = 74, },
    .{ .name = "K"              , .value = 75, },
    .{ .name = "L"              , .value = 76, },
    .{ .name = "M"              , .value = 77, },
    .{ .name = "N"              , .value = 78, },
    .{ .name = "O"              , .value = 79, },
    .{ .name = "P"              , .value = 80, },
    .{ .name = "Q"              , .value = 81, },
    .{ .name = "R"              , .value = 82, },
    .{ .name = "S"              , .value = 83, },
    .{ .name = "T"              , .value = 84, },
    .{ .name = "U"              , .value = 85, },
    .{ .name = "V"              , .value = 86, },
    .{ .name = "W"              , .value = 87, },
    .{ .name = "X"              , .value = 88, },
    .{ .name = "Y"              , .value = 89, },
    .{ .name = "Z"              , .value = 90, },
    .{ .name = "LEFT_BRACKET"   , .value = 91, },
    .{ .name = "BACKSLASH"      , .value = 92, },
    .{ .name = "RIGHT_BRACKET"  , .value = 93, },
    .{ .name = "GRAVE"          , .value = 96, },
    .{ .name = "SPACE"          , .value = 32, },
    .{ .name = "ESCAPE"         , .value = 256, },
    .{ .name = "ENTER"          , .value = 257, },
    .{ .name = "TAB"            , .value = 258, },
    .{ .name = "BACKSPACE"      , .value = 259, },
    .{ .name = "INSERT"         , .value = 260, },
    .{ .name = "DELETE"         , .value = 261, },
    .{ .name = "RIGHT"          , .value = 262, },
    .{ .name = "LEFT"           , .value = 263, },
    .{ .name = "DOWN"           , .value = 264, },
    .{ .name = "UP"             , .value = 265, },
    .{ .name = "PAGE_UP"        , .value = 266, },
    .{ .name = "PAGE_DOWN"      , .value = 267, },
    .{ .name = "HOME"           , .value = 268, },
    .{ .name = "END"            , .value = 269, },
    .{ .name = "CAPS_LOCK"      , .value = 280, },
    .{ .name = "SCROLL_LOCK"    , .value = 281, },
    .{ .name = "NUM_LOCK"       , .value = 282, },
    .{ .name = "PRINT_SCREEN"   , .value = 283, },
    .{ .name = "PAUSE"          , .value = 284, },
    .{ .name = "F1"             , .value = 290, },
    .{ .name = "F2"             , .value = 291, },
    .{ .name = "F3"             , .value = 292, },
    .{ .name = "F4"             , .value = 293, },
    .{ .name = "F5"             , .value = 294, },
    .{ .name = "F6"             , .value = 295, },
    .{ .name = "F7"             , .value = 296, },
    .{ .name = "F8"             , .value = 297, },
    .{ .name = "F9"             , .value = 298, },
    .{ .name = "F10"            , .value = 299, },
    .{ .name = "F11"            , .value = 300, },
    .{ .name = "F12"            , .value = 301, },
    .{ .name = "LEFT_SHIFT"     , .value = 340, },
    .{ .name = "LEFT_CONTROL"   , .value = 341, },
    .{ .name = "LEFT_ALT"       , .value = 342, },
    .{ .name = "LEFT_SUPER"     , .value = 343, },
    .{ .name = "RIGHT_SHIFT"    , .value = 344, },
    .{ .name = "RIGHT_CONTROL"  , .value = 345, },
    .{ .name = "RIGHT_ALT"      , .value = 346, },
    .{ .name = "RIGHT_SUPER"    , .value = 347, },
    .{ .name = "KB_MENU"        , .value = 348, },
    .{ .name = "KP_0"           , .value = 320, },
    .{ .name = "KP_1"           , .value = 321, },
    .{ .name = "KP_2"           , .value = 322, },
    .{ .name = "KP_3"           , .value = 323, },
    .{ .name = "KP_4"           , .value = 324, },
    .{ .name = "KP_5"           , .value = 325, },
    .{ .name = "KP_6"           , .value = 326, },
    .{ .name = "KP_7"           , .value = 327, },
    .{ .name = "KP_8"           , .value = 328, },
    .{ .name = "KP_9"           , .value = 329, },
    .{ .name = "KP_DECIMAL"     , .value = 330, },
    .{ .name = "KP_DIVIDE"      , .value = 331, },
    .{ .name = "KP_MULTIPLY"    , .value = 332, },
    .{ .name = "KP_SUBTRACT"    , .value = 333, },
    .{ .name = "KP_ADD"         , .value = 334, },
    .{ .name = "KP_ENTER"       , .value = 335, },
    .{ .name = "KP_EQUAL"       , .value = 336, },
    .{ .name = "BACK"           , .value = 4,  },
    .{ .name = "MENU"           , .value = 82, },
    .{ .name = "VOLUME_UP"      , .value = 24, },
    .{ .name = "VOLUME_DOWN"    , .value = 25, },
};
// zig fmt: on

pub fn getValueFromInputName(name: []const u8) ?i32 {
    for (input_codes) |ic| {
        if (std.mem.eql(u8, ic.name, name)) {
            return ic.value;
        }
    }
    return null;
}

pub fn initWindow(width: i32, height: i32, title: [*c]const u8, fps: i32) void {
    c.InitWindow(width, height, title);
    c.SetTargetFPS(fps);
}

pub fn windowShouldClose() bool {
    return c.WindowShouldClose();
}

pub fn closeWindow() void {
    c.CloseWindow();
}

pub fn beginDrawing() void {
    c.BeginDrawing();
}

pub fn endDrawing() void {
    c.EndDrawing();
}

pub fn getKeyCode(comptime name: []const u8) i32 {
    const full_name = "KEY_" ++ name;

    if (!@hasDecl(c, full_name)) {
        @compileError(full_name ++ " keycode does not exist.");
    }

    return @field(c, full_name);
}

pub fn isKeyPressed(key: i32) bool {
    return c.IsKeyPressed(key);
}

pub fn isKeyDown(key: i32) bool {
    return c.IsKeyDown(key);
}

pub fn isKeyReleased(key: i32) bool {
    return c.IsKeyReleased(key);
}

pub fn drawText(text: [*c]const u8, x: i32, y: i32, fontSize: i32, color: [4]u8) void {
    c.DrawText(text, x, y, fontSize, .{ .r = color[0], .g = color[1], .b = color[2], .a = color[3] });
}

pub fn setConfigFlags(flag: u32) void {
    c.SetConfigFlags(flag);
}

// Call before initializing the window
pub const ConfigFlag = enum(u32) {
    vsync_hint = 0x00000040, // set to try enabling v-sync on gpu
    fullscreen_mode = 0x00000002, // set to run program in fullscreen
    window_resizable = 0x00000004, // set to allow resizable window
    window_undecorated = 0x00000008, // set to disable window decoration (frame and buttons)
    window_hidden = 0x00000080, // set to hide window
    window_minimized = 0x00000200, // set to minimize window (iconify)
    window_maximized = 0x00000400, // set to maximize window (expanded to monitor)
    window_unfocused = 0x00000800, // set to window non focused
    window_topmost = 0x00001000, // set to window always on top
    window_always_run = 0x00000100, // set to allow windows running while minimized
    window_transparent = 0x00000010, // set to allow transparent framebuffer
    window_highdpi = 0x00002000, // set to support highdpi
    window_mouse_passthrough = 0x00004000, // set to support mouse passthrough, only supported when flag_window_undecorated
    borderless_windowed_mode = 0x00008000, // set to run program in borderless windowed mode
    msaa_4x_hint = 0x00000020, // set to try enabling msaa 4x
    interlaced_hint = 0x00010000, // set to try enabling interlaced video format (for v3d)

    pub fn set(self: ConfigFlag) void {
        setConfigFlags(@intFromEnum(self));
    }
};

pub fn clearBackground(color: [4]u8) void {
    c.ClearBackground(.{ .r = color[0], .g = color[1], .b = color[2], .a = color[3] });
}

pub fn drawTriangle(v1: [2]f32, v2: [2]f32, v3: [2]f32, color: [4]u8) void {
    c.DrawTriangle(
        .{ .x = v1[0], .y = v1[1] },
        .{ .x = v2[0], .y = v2[1] },
        .{ .x = v3[0], .y = v3[1] },
        .{ .r = color[0], .g = color[1], .b = color[2], .a = color[3] },
    );
}

pub const Texture = struct {
    texture: c.Texture2D,

    pub fn load(file_name: [*c]const u8) Texture {
        const aux = .{
            .texture = c.LoadTexture(file_name),
        };
        c.SetTextureFilter(aux.texture, c.TEXTURE_FILTER_BILINEAR);
        return aux;
    }

    pub fn drawTo(self: Texture, x: i32, y: i32, w: i32, h: i32, tint: [4]u8) void {
        const srcRec: c.Rectangle = .{
            .x = 0,
            .y = 0,
            .width = @floatFromInt(self.texture.width),
            .height = @floatFromInt(self.texture.height),
        };

        const dstRec: c.Rectangle = .{
            .x = @floatFromInt(x),
            .y = @floatFromInt(y),
            .width = @floatFromInt(w),
            .height = @floatFromInt(h),
        };

        c.DrawTexturePro(
            self.texture,
            srcRec,
            dstRec,
            .{ .x = 0, .y = 0 },
            0,
            .{ .r = tint[0], .g = tint[1], .b = tint[2], .a = tint[3] },
        );
    }

    pub fn unload(self: Texture) void {
        c.UnloadTexture(self.texture);
    }
};

pub fn getScreenWidth() i32 {
    return c.GetScreenWidth();
}

pub fn getScreenHeight() i32 {
    return c.GetScreenHeight();
}

pub fn getTime() f64 {
    return c.GetTime();
}

pub fn drawFPS(x: i32, y: i32) void {
    c.DrawFPS(x, y);
}
