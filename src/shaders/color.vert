#version 330 core

layout (location = 0) in vec2 pos;
layout (location = 1) in vec4 vColor;
out vec4 color;

uniform mat4 mvp;

void main() {
     color = vColor;
     gl_Position = mvp * vec4((pos + vec2(0.5, 0.5)), 0, 1);
}
