#version 330 core

in vec4 color;
in vec2 texCoord;
in float texId;

uniform sampler2D tex;

void main(){
     vec4 finalColor = color;

     if (int(texId) == 1) {
       finalColor *= texture(tex, texCoord);
     }

     gl_FragColor = finalColor;
}
