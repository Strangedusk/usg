#version 330 core

layout (location = 0) in vec2 pos;
layout (location = 1) in vec4 vColor;
layout (location = 2) in vec2 vTexCoord;
layout (location = 3) in float vTexId;
out vec4 color;
out vec2 texCoord;
out float texId;

uniform mat4 mvp;

void main() {
     texCoord = vTexCoord;
     color = vColor;
     texId = vTexId;
     gl_Position = mvp * vec4((pos + vec2(0.5, 0.5)), 0, 1);
}
